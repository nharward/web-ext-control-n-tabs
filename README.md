# web-ext-control-n-tabs

Web Extension to activate tabs by using Control+&lt;number>. The Firefox addon page is [here](https://addons.mozilla.org/en-US/firefox/addon/control-number-tab-switcher/).

# Why

I use `Alt+<number>` to switch workspaces, and it annoyed me that it wasn't easy to change the tab selection hotkey in Firefox to `Control+<number>`. All the existing key remapping plugins seemed a bit much to me so I decided to write this just for myself. If you like it too please use it :)
